import SocketIOClient from 'socket.io-client';
import config from './../config';
import { Constants } from 'expo';

export default (async function() {
    console.log('start connection with websocket');

    let socket = SocketIOClient(config.socket_url, {
        query: {
            device_id: Constants.deviceId,
        }
    });

    socket.on('connect', function() {
        console.log('Connected with websocket');
    });

    return socket;
});