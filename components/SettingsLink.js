import React, { Component } from 'react';
import { Linking, Text } from 'react-native';

export default class SettingsLink extends Component {
    render() {
        return <Text style={{color: 'blue', textDecorationLine: 'underline', marginTop: 10, marginBottom: 10}} onPress={this._handlePress}>{this.props.name}</Text>
    }

    _handlePress = () => {
        Linking.openURL(this.props.link);
        this.props.onPress && this.props.onPress();
    };
}
