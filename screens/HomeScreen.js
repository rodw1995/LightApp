import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    View,
    Alert
} from 'react-native';

import crypto from 'crypto-js';
import config from './../config';
import postBuy from './../api/postBuy';

import { Permissions, BarCodeScanner} from 'expo';

export default class HomeScreen extends Component {

    state = {
        hasCameraPermission: null,
    };

    static navigationOptions = {
        header: null,
    };

    render() {
        let content = null;
        if (this.props.screenProps.barCodeScanned !== null) {
            if (this.props.screenProps.barCodeScanned === false && this.props.screenProps.scanning === false) {
                content = <View style={styles.contentContainer}>
                    <View>
                        {this.state.hasCameraPermission === null ?
                            <Text>Requesting for camera permission</Text> :
                            this.state.hasCameraPermission === false ?
                                <Text>Camera permission is not granted</Text> :
                                <BarCodeScanner
                                    onBarCodeRead={this._handleBarCodeRead}
                                    style={{ height: 200, width: 200 }}
                                />
                        }
                    </View>
                    <Text style={{margin: 15}}>Scan the code on light.rodw.nl and support the light with 5 free minutes!</Text>
                </View>;
            } else if (this.props.screenProps.scanning) {
                // Currently handling the scan
                content = <View style={styles.contentContainer}>
                    <Text style={{margin: 15}}>
                        Scanned code successfully, please wait a moment...
                    </Text>
                </View>;
            } else {
                content = <View style={styles.contentContainer}>
                    <Text style={{margin: 15}}>
                        You already have scanned the free code in the last hour.
                        You can buy extra light time with Gulden!
                    </Text>
                </View>;
            }
        }

        return (
            <View style={styles.container}>
                <ScrollView
                    style={styles.container}
                    contentContainerStyle={styles.contentContainer}>

                    <Text>Current light state: {this.props.screenProps.lightOn ?
                        <Text style={{color: '#37D67A'}}>ON</Text> :
                        <Text style={{color: '#F47373'}}>OFF</Text>}
                    </Text>

                    {content}
                </ScrollView>
            </View>
        );
    }

    componentDidMount() {
        this._requestCameraPermission();
    }

    _requestCameraPermission = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({
            hasCameraPermission: status === 'granted',
        });
    };

    _handleBarCodeRead = async (data) => {
        if (data.type === 'QR_CODE' && this.props.screenProps.barCodeScanned === false) {
            // Scanning -> we are currently scanning
            this.props.screenProps.scanningStep();

            // Encrypt and post
            let hash = crypto.AES.encrypt(data.data, config.shared_secret);

            if (await postBuy(hash.toString()) === true) {
                Alert.alert('Success', 'Thank you for the light!');
                this.props.screenProps.toggleBarCodeScanned();
            } else {
                Alert.alert('Error', 'Wrong QR code, try again');
            }

            // Scanning is ended
            this.props.screenProps.scanningStep();
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    contentContainer: {
        marginTop: 40,
        alignItems: 'center',
    },
});
