import React from 'react';
import { ScrollView, StyleSheet, View, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import SettingsLink from './../components/SettingsLink';

export default class SettingsScreen extends React.Component {
    static navigationOptions = {
        header: null
    };

    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.contentContainer}>
                    <Text style={{fontWeight: 'bold', fontSize: 16}}>Awesome app by Robbie op de Weegh</Text>
                    <View style={styles.linksTitle}>
                        <FontAwesome name="gitlab" size={32}/>
                        <Text style={{paddingLeft: 10, fontSize: 14}}>Gitlab Links</Text>
                    </View>
                    <SettingsLink name="React Native App" link="https://gitlab.com/rodw1995/LightApp"/>
                    <SettingsLink name="NodeJS server" link="https://gitlab.com/rodw1995/LightServer"/>
                    <SettingsLink name="React Client" link="https://gitlab.com/rodw1995/LightClient"/>
                    <SettingsLink name="Arduino Project" link="https://gitlab.com/rodw1995/LightArduino"/>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    contentContainer: {
        alignItems: 'center',
        marginTop: 40,
    },
    linksTitle: {
        display: 'flex',
        marginTop: 50,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    }
});
