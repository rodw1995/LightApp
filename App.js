import React from 'react';
import { Platform, StatusBar, StyleSheet, View, Alert } from 'react-native';

import { AppLoading } from 'expo';
import { FontAwesome } from '@expo/vector-icons';
import { Constants } from 'expo';

import RootNavigation from './navigation/RootNavigation';
import cacheAssetsAsync from './utilities/cacheAssetsAsync';

import connectWithSocket from './websocket/connect';
import isLightOn from './api/isLightOn';
import canScan from './api/canScan';

export default class AppContainer extends React.Component {

    state = {
        appIsReady: false,
        isLightOn: false,
        barCodeScanned: null,
        scanning: false,
    };

    constructor(props) {
        super(props);
        this.scanningStepCounter = 0;
    }

    toggleBarCodeScanned() {
        this.setState({
            barCodeScanned: this.state.barCodeScanned === false
        })
    }

    scanningStep() {
        this.scanningStepCounter++;

        if (this.scanningStepCounter === 3) {
            this.scanningStepCounter = 0;
        }

        this.setState({
            scanning: this.scanningStepCounter > 0,
        });
    }

    componentWillMount() {
        this._loadAssetsAsync();
    }

    render() {
        if (this.state.appIsReady) {
            return (
                <View style={styles.container}>
                    {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                    {Platform.OS === 'android' &&
                    <View style={styles.statusBarUnderlay} />}
                    <RootNavigation
                        lightOn={this.state.isLightOn}
                        barCodeScanned={this.state.barCodeScanned}
                        scanning={this.state.scanning}
                        toggleBarCodeScanned={this.toggleBarCodeScanned.bind(this)}
                        scanningStep={this.scanningStep.bind(this)}
                    />
                </View>
            );
        } else {
            return <AppLoading />;
        }
    }

    componentDidMount() {
        this._setLightOnState();
        this._setBarCodeScannedState();
        this._setUpWebSocket();
    }

    async _loadAssetsAsync() {
        try {
            await cacheAssetsAsync({
                images: [require('./assets/images/expo-wordmark.png')],
                fonts: [
                    FontAwesome.font,
                    { 'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf') },
                ],
            });
        } catch (e) {
            console.warn(
                'There was an error caching assets (see: main.js), perhaps due to a ' +
                'network timeout, so we skipped caching. Reload the app to try again.'
            );
            console.log(e.message);
        } finally {
            this.setState({ appIsReady: true });
        }
    }

    _setLightOnState = async () => {
        this.setState({
            isLightOn: await isLightOn(),
        });
    };

    _setBarCodeScannedState = async () => {
        this.setState({
            barCodeScanned: !await canScan(),
        });
    };

    _setUpWebSocket = async () => {
        this.socket = await connectWithSocket();

        this.socket.on('light_changed', (data) => {
            this.setState({
                isLightOn: data.light.on,
            });

            if (this.state.scanning > 0) {
                // Scan by this device -> scanning
                this.scanningStep();
            } else if (this.state.isLightOn !== data.light.on) {
                Alert.alert(
                    'State changed',
                    (data.light.on ? 'The light is now on!' : 'The light is now off'),
                );
            }
        });

        this.socket.on('can_scan_again', (deviceId) => {
            // Not really needed because the socket will only emit this message to the specific socket
            // But just for extra security....
            if (deviceId === Constants.deviceId) {
                this.setState({
                    barCodeScanned: false,
                });
            }
        });
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    statusBarUnderlay: {
        height: 24,
        backgroundColor: 'rgba(0,0,0,0.2)',
    },
});
