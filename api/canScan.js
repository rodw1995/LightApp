import config from './../config';
import { Constants } from 'expo';

export default (async function canScan() {
    try {
        let response = await fetch(config.can_scan_api + '/' + Constants.deviceId);
        let responseJson = await response.json();

        return !responseJson.result;
    } catch(error) {
        console.error(error);
    }
});