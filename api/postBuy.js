import config from './../config';
import { Constants } from 'expo';

export default (async function postBuy(token) {
    try {
        let response = await fetch(config.light_buy_api, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                token: token,
                device_id: Constants.deviceId,
            })
        });
        let responseJson = await response.json();

        return responseJson.state === 'success';
    } catch(error) {
        console.error(error);
    }
});
